<?php
class Tienda{
        function __construct(){
            //echo "Clase generada";
            session_start();
        }

    public function home(){

        $_SESSION['error']="<h3>Rellene los datos a continuacion</h3>";
        if (empty($_SESSION['user'])) {
            $_SESSION['user'] = "-invitado-";
        } else if (!isset($_SESSION['precio'])){
            $_SESSION['precio'] = 0;
        }
        require ('home.php');
    }

    public function login(){
        require ('loginView.php');
    }

    public function logearse(){
        $user = $_REQUEST['user'];
        $pwd = $_REQUEST['pwd'];

        if(isset($user) && !empty($user) && isset($pwd) && !empty($pwd)){
            $_SESSION['user'] = $user;

            if(isset($_REQUEST['recordarme']))
            {
                setcookie('user', $user);
                setcookie('pwd', $pwd);
            } else {
                setcookie('user', $user, time() -1);
                setcookie('pwd', $pwd, time()-1);
            }
            header('Location:?method=home');

        } else {
            $_SESSION['error'] = "<h5 style='color: red';>Usuario o contraseña incorrectos</h5>";
            header('Location:?method=login');
        }
    }

    public function sign_in(){
        require('sign_inView.php');
    }


    public function comprobar(){
        //echo "metodo para registrarse";
        $user = $_REQUEST['user'];
        $pwd = $_REQUEST['pwd'];
        $mail = $_REQUEST['mail'];
        //$prueba = $this->validar($user);
        if (isset($user) && !empty($user) && isset($mail) && !empty($mail)) {
            if ($this->validar('mail') && $this->validar('user')) {
                $_SESSION['users'][] = $user; //probar SESSION[] = [cosa];
                $_SESSION['mails'][] = $mail;
                $_SESSION['error'] = "Registro completado con exito";
            } else {
                $_SESSION['error'] = "Usuario o correo ya registrado";
            }
        } else {
            $_SESSION['error'] = "";
        }

        header('Location:?method=sign_in');
        //echo "$prueba";

    }

    public function validar($parameter){//no cumple su funcion
        echo "funcion validar";
        $array = $_SESSION[$parameter];
        foreach ($array as $key => $value) {
            if($value.search($this)>=0)
            {
                return false;
            }
            echo "$value";
        }

        return true;
    }

    public function logout() {
        unset($_SESSION['user']);

        header('Location:?method=home');
    }

    public function wishlist() {
        if (empty($_SESSION['user'])) {

        }
    }
}//clase Tienda





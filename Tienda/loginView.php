<?php
    require ("cookie.php");
 ?>

<!DOCTYPE html>
<html>
<head>
    <title>Inciar Sesion</title>
    <?php
        require("estilos.php");
     ?>
</head>
<body>
    <?php
        require ("schema.php");
    ?>
    <div id="body">
        <h2>Log in</h2>
        <?php echo "$_SESSION[error]" ?>
        <form method="POST" action="index.php?method=logearse">
            <label>Usuario</label><input type="text" name="user" value="<?php echo $user ?>"><br>
            <label>Contraseña</label><input type="password" name="pwd" value="<?php echo $pwd ?>"><br>
            <input type="submit" name="submit">
            <input type="checkbox" name="recordarme">Recordarme
        </form>

        ¿Aun no esta registrado? <a href="index.php?method=sign_in">Registrese aqui</a>
    </div>
</body>
</html>

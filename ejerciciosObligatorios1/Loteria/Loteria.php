<?php

class Loteria
{
    function __construct()
    {
        session_start();

    }

    //pagina principal de nuestro proyecto
    function verCarton()
    {
        $carton;
        for ($i=1; $i <= 49 ; $i++) {
            $carton[$i-1]=$i;
        }

        if (!isset($_SESSION['contador'])) {
            $_SESSION['contador'] = 0;
        }  if (!isset($_SESSION['error'])) {
            $_SESSION['error'] = "Bienvenido!";
        }


        require('vistaApuesta.php');
    }

    //añade un numero a nuestro boleto
    function añadirApuesta() //en principio tendria que añadir
    {

        $indice = $_REQUEST['numero'] + 1;
        $apuestas = $_SESSION['apuestas'];

        //tenemos que comprobar previamente si ya hemos apostado a ese numero
        foreach ($apuestas as $apuesta) {
            if($apuesta == $indice)
            {

                $encontrado = true;//numero metido ya en la apuesta
            }
        }

        if (isset($_SESSION['apuestas'] )) //si existe ya alguna apuesta
        {
            if($encontrado == false)//y no encontramos el numero en la apuesta
            {
                $_SESSION['apuestas'][] = $_REQUEST['numero'] + 1;
                $_SESSION['contador'] = $_SESSION['contador'] + 1;//metemos el numero en la misma
                $_SESSION['error'] = "Apuesta realizada";
            } else {
                $_SESSION['error'] = "Numero ya incluido en la apuesta";
                //imprimira mensaje por pantalla
            }

        } else {
            $_SESSION['apuestas'] = [];//se genera un array
        }

        header("Location:index.php?method=verCarton");
    }

    //borra una apuesta especifica
    function borrarApuesta()
    {

        $indice = $_REQUEST['indice'];
        $apuestas = $_SESSION['apuestas'];

        $_SESSION['contador'] = $_SESSION['contador'] - 1;//restamos a contador

        unset($_SESSION['apuestas'][$_REQUEST['indice']]);
        //quitamos el indice a borrar del array apuestas

        header("Location:index.php?method=verCarton");
    }

    //elimina toda la apuesta acumulada
    function borrarTodo()
    {
        unset($_SESSION['apuestas']);
        unset($_SESSION['contador']);
        header("Location:index.php?method=añadirApuesta");
    }

}

<!DOCTYPE html>
<html>
<head>
    <title>Introduccion</title>
</head>
<body>
    <h1>3. Lotería primitiva</h1>
    <h3>Método index:</h3>
    <p>Muestra una tabla 7 filas x 7 columnas, con los 49 números de la lotería. Es la única vista del ejercicio.</p>

    <h3>Método toggle:</h3>
    <p>Cada número tiene un enlace a otro método que añade/quita números de la apuesta. La apuesta se guarda en sesión (array apuesta[]). Hay que informar al usuario de los números apostados. Por ejemplo: Lista de números jugados, fondo de color, etc</p>
    <ul>
        <li>Si los números marcados no llegan a 6 debe informarse de apuesta incompleta.</li>
        <li>Si los números marcados son exactamente 6 debe indicarse apuesta SIMPLE</li>
        <li>Si los números marcados son más de 6 debe indicarse apuesta MULTIPLE (y el número de apuestas)</li>
    </ul>
    <a href="index.php?method=verCarton">A apostar!!</a>

</body>
</html>

<?php

class Galeria
{
    function __construct()
    {
        session_start();
    }

    function home()
    {
        $this->directorio();
        if (empty($_SESSION['msg'])) {
            $_SESSION['msg'] = "Bienvenido!";
        }
        require ("vistaGaleria.php");
    }

    function foto ()
    {
        $_SESSION['foto'] = $_REQUEST['key'];

        require("vistaFoto.php");
    }

    function subir()
    {
        $subido = true;
        $uploadedfile_size = $_FILES['ficheroSubido']['size'];
        echo $_FILES['ficheroSubido']['name'];
        if ($_FILES['ficheroSubido']['size'] > 200000) {
            $_SESSION['msg'] = "El archivo es mayor que 200KB, debes reduzcirlo antes de subirlo";
            $subido = false;
        }


        $file_name = $_FILES['ficheroSubido']['name'];
        //puede que aqui este el error
        $add="imagenes/$file_name";


        if ($subido) {
            if (move_uploaded_file($_FILES['ficheroSubido']['tmp_name'], $add)) {
                $ruta = "$add$file_name";
                $_SESSION['msg'] = " Ha sido subido satisfactoriamente";
            } else {
                $_SESSION['msg'] = "Error al subir el archivo";
            }
        }

        header("Location:?method=home");
    }

    function directorio ()
    {

        $_SESSION['directorio'] = [];
        $directorio = opendir("imagenes/"); //ruta actual
        //obtenemos un archivo y luego otro sucesivamente
        while ($archivo = readdir($directorio)) {
        //verificamos si es o no un directorio
            if (is_dir($archivo)) {


            } else {
                $_SESSION['directorio'][] = $archivo;
            //echo $archivo . "</br>";
            }
        }
    }


    function borrar()
    {
        $borrar = $_REQUEST['key'];
        $ruta = "imagenes/" . $borrar;

        unlink($ruta);
        $_SESSION['msg'] = "Foto borrada";
        header('Location:?method=home');
    }

}


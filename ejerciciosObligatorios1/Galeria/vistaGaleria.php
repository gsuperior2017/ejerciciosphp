<!DOCTYPE html>
<html>
<head>
    <title>Mi Galeria</title>
</head>
<body>
    <h3><?php echo "$_SESSION[msg]" ?></h3>
    <form enctype="multipart/form-data" action="index.php?method=subir" method="post">
        <input name="ficheroSubido" type="file" />
        <input type="submit" value="Subir archivo" />

    </form>
    <br>
    <h3>Galeria de imagenes</h3>
    <hr>
    <?php
    if (isset($_SESSION['directorio']) && !empty($_SESSION['directorio'])) {
        foreach ($_SESSION['directorio'] as $indice => $foto): ?>
        <a href="?method=foto&key=<?php echo $foto ?>"><img width="100px" src="imagenes/<?php echo $foto ?>" ></a>
        <a href="?method=borrar&key=<?php echo $foto ?>"> Borrar </a>
        <br> <?php endforeach;
    } else {
        echo "No hay fotos";
    } ?>
    <br>
    <hr>

</body>
</html>

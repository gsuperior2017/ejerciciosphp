<?php

class Calculadora
{
    function __construct()
    {
        //echo "Clase Calculadora generada!";
        session_start();


    }

    //primera vista que recibe el cliente tras pasar por index
    public function operacion()
    {
        if(empty($_SESSION['error'])) //en primera instancia no entra, ya que no ha sido declarada
        {
            $_SESSION['error'] = "Error en los valores introducidos";

        } else {
            $_SESSION['error'] = "Bienvenido!";
        }
        require('vistaOperacion.php');
    }

    //metodo que generara la respuesta a cliente, acorde con los parametros recibidos
    public function resultado()
    {

        $operador1 = $this->comprobar($_REQUEST['numero1']);
        $operador2 = $this->comprobar($_REQUEST['numero2']);
        $operador = $_REQUEST['operacion'];
        $resultado = $this->operar($operador1,$operador2, $operador);

        require('vistaResultado.php');
    }

    //metodo que comprueba si el valor introducido es un numero
    public function comprobar ($operador)
    {

        if(is_numeric($operador) && !empty($operador) && isset($operador) || $operador == "0")
        {

            return $operador;
        } else {

            $_SESSION['error'] = "";
            //forzamos a que al volver al metodo salga el error por pantalla

            header('Location:./index.php?method=operacion');
        }
    }

    //resolvemos la operacion y almacenamos variables necesarias
    public function operar($a, $b, $operacion)
    {
        if($operacion == "Suma")
        {

            $c = $a + $b;
            $_SESSION['signo'] = " + ";
            //guardamos en sesion el parametro para usarlo en la vista final

            return $c;
        }
        else if ($operacion == "Resta")
        {
            $c = $a - $b;
            $_SESSION['signo'] = " - ";
            return $c;
        }
        else if ($operacion == "Division")
        {
            //salvamos el error de que no pueda hacer operaciones del tipo 0/0
            $_SESSION['signo'] = " / ";
            if($b == 0)
            {
                return "Math Error";
            } else {
                $c = $a / $b;
                return $c;
            }

        } else {
            $c = $a * $b;
            $_SESSION['signo'] = " * ";
            return $c;

        }
    }
}



<?php
$semana = ["LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO"];

 ?>

<!DOCTYPE html>
<html>
<head>
    <title> ejercicio3 </title>
</head>
<body>
    <p>Dias de la semana:</p>
    <ul>
        <?php

        foreach ($semana as $dia) {
            echo "<li> $dia </li>";
        }

         ?>

    </ul>
    <hr>
    De otro modo:
    <ul>
        <?php

        foreach ($semana as $numero => $dia) {
            echo "<li>Dia " . ++$numero . ": $dia </li>";
        }

         ?>

    </ul>

    <a href="index.php">- Volver- </a>
</body>
</html>

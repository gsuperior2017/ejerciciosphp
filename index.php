<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios php</title>
</head>
<body>
    <h1>Ejercicios PHP</h1>
    <h2>Alumno: Alejandro Torrellas</h2>
    Crea enlaces a tus ejercicios:
    <ul>
        <li><a href="ejercicios1-8/ejercicio1.php"> Ejercicio 1</a></li>
        <li><a href="ejercicios1-8/ejercicio2.php"> Ejercicio 2</a></li>
        <li><a href="ejercicios1-8/ejercicio3.php"> Ejercicio 3</a></li>
        <li><a href="ejercicios1-8/ejercicio4.php"> Ejercicio 4</a></li>
        <li><a href="formularios/formularioMuestra.html"> Ejercicio formularioMuestra</a></li>
        <li><a href="ejercicios1-8/ejercicio5.html"> Ejercicio 5 y 6</a></li>
        <!-- <li><a href="/ejercicio7.php"> Ejercicio 7</a></li> -->
        <li><a href="ejercicios1-8/ejercicio7resuelto.php"> Ejercicio 7</a></li>
        <li><a href="cookies/cookie.php"> Cookies</a></li>
        <li><a href="ejerciciosObligatorios1/Galeria"> Galeria</a></li>
        <li><a href="ejerciciosObligatorios1/Loteria"> Loteria</a></li>
        <li><a href="ejerciciosObligatorios1/Calculadora"> Calculadora</a></li>
        <li><a href="Tienda"> Tienda</a></li>


    </ul>
</body>
</html>

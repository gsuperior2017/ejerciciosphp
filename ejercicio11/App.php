<?php


class App
{

    function __construct()
    {
        echo "App!<br>";
        session_start();

    }

    public function login()
    {
        require('viewLogin.php');
    }

    public function auth()
    {
        echo "auth!!<br>";

        if (isset($_REQUEST['user']) && !empty($_REQUEST['user']))
        {
            $user = $_REQUEST['user'];
            $_SESSION['user'] = $user;
            header('Location:index.php?method=home');
        } else {
            header('Location:index.php?method=login');
        }

        return;
    }

    public function home()
    {
        if (!isset($_SESSION['user']))
        {
            header('Location:index.php?method=login');
            return;
        }


        else
        {
            if (isset($_SESSION['deseos']))
            {
                $deseos = $_SESSION['deseos'];
            } else {
                $deseos = [];
            }

            require('viewList.php');
        }
    }

    public function new()
    {
        if(isset($_REQUEST['deseo']) && !empty($_REQUEST['deseo']))
        {
            $_SESSION['deseos'][] = $_REQUEST['deseo']; /*para añadir elementos*/
        }

        header("Location:?method=home");
    }

    public function close()
    {
        session_destroy();
        unset($_SESSION);

        header('Location:?method=login');
    }

    public function delete ()
    {
        echo "Elemento borrado";
        $key = (integer) $_REQUEST['key'];
        unset($_SESSION['deseos'][$key]);
        header("Location:?method=home");

    }

    public function deleteAll ()
    {
        /*foreach ($deseos as $key => $deseo) {
            echo "Elemento $key borrado";
            $key = (integer) $_REQUEST['key'];
            unset($_SESSION['deseos'][$key]);
            header("Location:?method=home");
        }*/

        unset($_SESSION['deseos']);
        header("Location:?method=home");
    }
}
